import Phaser from "phaser";

export default class Ship extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, type, speed=100){
        super(scene, x, y, type);
        this.scene = scene;
        this.speed = speed;
        scene.physics.add.existing(this);
        scene.add.existing(this);
        scene.enemis.add(this);

        this.play(`${type}_anim`);

        // need to set velocity
        scene.physics.world.enableBody(this);
        this.body.velocity.y = speed;
    }

    resetShipPos(){
        this.y = 0;
        let randomX = Phaser.Math.Between(0,  Number(this.scene.game.config.width));
        this.x = randomX;
    }

    update(){
        if (this.y > this.scene.game.config.height + 200) {
            this.resetShipPos();
            this.scene.downMainHealth(5);
        }
    }
}