import Phaser from "phaser";

export default class Beam extends Phaser.GameObjects.Sprite{
    constructor(scene){
        let x = scene.player.x;
        let y = scene.player.y - 14;
        super(scene, x, y, "beam");
        // add to group
        scene.projectiles.add(this);
        // add to scene
        scene.add.existing(this);

        this.play("beam_anim");

        // need to set velocity
        scene.physics.world.enableBody(this);
        this.body.velocity.y = -250;
    }

    update() {
        if(this.y < -1){
            this.destroy();
        }
    }

}