import Phaser from 'phaser';
import { gameSettings } from "../settings";
import Beam from '../components/Beam';
import Explosion from '../components/Explosion';
import Ship from '../components/Ship';


class Scene2 extends Phaser.Scene {
    constructor() {
        super("playGame");
        this.score = 0;
        this.maxPowerUps = 4;
        this.maxEnemies = 5;
        this.mainHealth = 100;
    }

    preload() {
        // pass
    }

    // destroyShip(pointer, gameObject) {
    //     // gameObject.setTexture("explosion");
    //     // gameObject.play("explode");
    //     // console.log(gameObject);
    //     let explosion = this.add.sprite(gameObject.x, gameObject.y, "explosion");
    //     explosion.play("explode");
    //     this.score += 1;
    //     this.scoreText.text = `Score: ${this.score}`;
    // }

    playerShoot(){
        let beam = new Beam(this);
    }

    movePlayerManager(){
        // this.cursorKeys.space
        // Phaser.Input.Keyboard.JustDown(this.key_a)
        let move = false;
        if(this.cursorKeys.left.isDown || this.key_a.isDown){
            this.player.setVelocityX(-gameSettings.playerSpeed);
            move = true;
        }else if (this.cursorKeys.right.isDown || this.key_d.isDown){
            this.player.setVelocityX(gameSettings.playerSpeed);
            move = true;
        }
        
        if(this.cursorKeys.up.isDown || this.key_w.isDown){
            this.player.setVelocityY(-gameSettings.playerSpeed);
            move = true;
        }else if (this.cursorKeys.down.isDown || this.key_s.isDown){
            this.player.setVelocityY(gameSettings.playerSpeed);
            move = true;
        }
        
        if(!move){
            this.player.setVelocityY(0);
            this.player.setVelocityX(0);
        }

        if(Phaser.Input.Keyboard.JustDown(this.spacebar)){
            this.playerShoot();
        }
        // this.input.once('pointerdown', pointer => {this.playerShoot();})

    }

    upScore(points) {
        this.score += points; 
        this.scoreText.text = `SCORE ${this.score}`;
    }

    downMainHealth(points){
        this.mainHealth -= points;
        this.mainHealthText.text = "-".repeat(this.mainHealth/4);
        if (this.mainHealth <= 0 ){
            this.add.text(
                Number(this.game.config.width)/2 - 40,
                Number(this.game.config.height)/2, 
                "YOU LOOSE", 
                {
                    fontSize: "100",
                    fontStyle: "strong",
                    color: "red"
                }
            );
        }
    }

    updateElementsOfGroup(group) {
        let groupElements  = group.getChildren();
        for(let i = 0; i < groupElements.length; i++){
            let beam = groupElements[i];
            beam.update();
        }
    }

    create() {
        // Создаем группы
        this.projectiles = this.add.group();
        this.powerUps = this.physics.add.group();
        this.enemis = this.physics.add.group({
            immovable: true,
            allowGravity: false
        });

        // this.background = this.add.image(0, 0, "background");
        this.background = this.add.tileSprite(0, 0, Number(this.game.config.width), Number(this.game.config.height), "background");
        this.background.setOrigin(0, 0);
        
        // this.ship1 = this.add.image(config.width/2, config.height/2, "ship");
        // this.ship2 = this.add.image(config.width/2 - 50, config.height/2, "ship2");
        // this.ship3 = this.add.image(config.width/2 + 50, config.height/2, "ship3");

        // Создаем спрайты кораблей
        // this.ship1 = this.physics.add.sprite(
        //     Number(this.game.config.width)/2,
        //     Number(this.game.config.height)/2,
        //     "ship"
        // );
        // this.ship2 = this.physics.add.sprite(
        //     Number(this.game.config.width)/2 - 50,
        //     Number(this.game.config.height)/2,
        //     "ship2"
        // );
        // this.ship3 = this.physics.add.sprite(
        //     Number(this.game.config.width)/2 + 50, 
        //     Number(this.game.config.height)/2,
        //     "ship3"
        // );
        // this.ship4 = this.physics.add.sprite(
        //     Number(this.game.config.width)/2 + 25,
        //     Number(this.game.config.height)/2 + 30,
        //     "ship4"
        // );
        // this.enemis.add(this.ship1);
        // this.enemis.add(this.ship2);
        // this.enemis.add(this.ship3);
        // this.enemis.add(this.ship4);
        // this.ship1.setImmovable();

        this.enemy_1 = new Ship(this, Number(this.game.config.width)/4-40, -20, "ship1", Phaser.Math.Between(30, 200));
        this.enemy_2 = new Ship(this, Number(this.game.config.width)/4*2-40, -20, "ship2", Phaser.Math.Between(30, 200));
        this.enemy_3 = new Ship(this, Number(this.game.config.width)/4*3-40, -20, "ship3", Phaser.Math.Between(30, 200));
        this.enemy_4 = new Ship(this, Number(this.game.config.width)/4*4-40, -20, "ship4", Phaser.Math.Between(30, 200));

        // Задаем спрайт корабля
        this.player = this.physics.add.sprite(
            Number(this.game.config.width)/2 - 8,
            Number(this.game.config.height) - 64,
            "player"
        );
        this.player.setCollideWorldBounds(true);

        // Спавним павер апы и заносим в одну группу
        // for (let i = 0; i <= this.maxPowerUps; i++){
        //     let powerUp = this.physics.add.sprite(16, 16, "power-up");
        //     this.powerUps.add(powerUp);
        //     powerUp.setRandomPosition(0, 0, Number(this.game.config.width), Number(this.game.config.height));

        //     if (Math.random() > 0.5){
        //         powerUp.play("red");
        //     }else{
        //         powerUp.play("gray");
        //     }

        //     powerUp.setVelocity(100, 100);
        //     powerUp.setCollideWorldBounds(true);
        //     powerUp.setBounce(1);
        // }

        // Задаем коллизию
        this.physics.add.overlap(
            this.player,
            this.enemis,
            (player, enemy) => {
                // @ts-ignore
                let explosion = new Explosion(this, player.x, player.y);
            }
        );

        this.physics.add.collider(
            this.projectiles,
            this.enemis,
            (projectile, enemy) => {
                // @ts-ignore
                let explosion = new Explosion(this, enemy.x, enemy.y);
                projectile.destroy();
                // @ts-ignore
                enemy.resetShipPos();
                this.upScore(1);
            }
        );

        // Старт анимаций
        this.player.play("thrust");

        // Корабли становятся интерактивными
        // this.ship1.setInteractive();
        // this.ship2.setInteractive();
        // this.ship3.setInteractive();
        // this.ship4.setInteractive();
        // Коллбэк для клика по по объекту
        // this.input.on("gameobjectdown", this.destroyShip, this);

        let graphics = this.add.graphics();
        graphics.fillStyle(0x000000, 1);
        graphics.moveTo(0, 0);
        graphics.lineTo(Number(this.game.config.width), 0);
        graphics.lineTo(Number(this.game.config.width), 20);
        graphics.lineTo(0, 20);
        graphics.lineTo(0, 0);
        graphics.closePath();
        graphics.fillPath();
        this.scoreText = this.add.text(5, 5, `SCORE ${this.score}`, {
            font: "35",
            color: "yellow"
        });

        this.mainHealthText = this.add.text(Number(this.game.config.width) - 100, 5, "-".repeat(this.mainHealth/4), {
            font: "20",
            color: "red"
        });

        // Менеджер нажатий стрелочек и пробела
        this.cursorKeys = this.input.keyboard.createCursorKeys();

        // Конкретные клавиши клавы
        this.spacebar = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
        this.key_w = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
        this.key_s = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
        this.key_a = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
        this.key_d = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
        
    }

    update() {
        // Смещает тайл бэграунда для анимации движения поверхности
        this.background.tilePositionY -= 0.5;
        this.updateElementsOfGroup(this.projectiles);
        this.updateElementsOfGroup(this.enemis);
        this.movePlayerManager();
    }
}


export default Scene2;